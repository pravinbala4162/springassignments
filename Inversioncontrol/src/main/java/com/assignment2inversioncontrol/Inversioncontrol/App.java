package com.assignment2inversioncontrol.Inversioncontrol;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class App 
{
    public static void main( String[] args )
    {ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
    
	
	
	player obj1 = (player) context.getBean("player1");
	player obj2 = (player) context.getBean("player2");
	player obj3 = (player) context.getBean("player3");
	player obj4= (player) context.getBean("player4");
	player obj5= (player) context.getBean("player5");
	
	
	System.out.print(obj1.playercountry());
	
    }
}
