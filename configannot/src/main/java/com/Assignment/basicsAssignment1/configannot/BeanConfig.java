package com.Assignment.basicsAssignment1.configannot;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages="com.Assignment.basicsAssignment1.configannot.Movie")


public class BeanConfig {

	
	@Bean
	public Movie obj()
	{
		return new Movie();
	}
	
	
	
	
}
