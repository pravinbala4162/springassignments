package com.Assignment.basicsAssignment1.configannot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;



public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfig.class);
    	   
        
    	
    	Movie obj = ctx.getBean(Movie.class);
        
    	
    	obj.setMovieId("M001");
    	obj.setMovieName("firstclass");
    	
    	obj.setMovieActor("tom");
    	
    	
    	System.out.print(obj.toString());
       
       
       
    }
}
