package com.assigmnetdependyin.inversion;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        
    	
    	
    	Student obj = (Student) context.getBean("obj");
        
    	System.out.print(obj.toString());
    	
	Student obj1 = (Student) context.getBean("obj1");
        
    	System.out.print(obj1.toString());
    	
    	
    	
    }
}
