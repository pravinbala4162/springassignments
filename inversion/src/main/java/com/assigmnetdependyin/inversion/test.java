package com.assigmnetdependyin.inversion;


import java.util.Map;

public class test {
	
	private  String testId;
	
	
	@Override
	public String toString() {
		return "test [testId=" + testId + ", testTitle=" + testTitle +  "]";
	}
	private Map<String,Integer> testTitle;
	
	
	public Map<String, Integer> getTestTitle() {
		return testTitle;
	}
	public void setTestTitle(Map<String, Integer> testTitle) {
		this.testTitle = testTitle;
	}

	
	public String getTestId() {
		return testId;
	}
	public void setTestId(String testId) {
		this.testId = testId;
	}
	
	

}
