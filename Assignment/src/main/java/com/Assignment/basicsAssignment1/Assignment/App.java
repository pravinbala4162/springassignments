package com.Assignment.basicsAssignment1.Assignment;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext context = new ClassPathXmlApplicationContext("hellomovie.xml");
        Movie obj = (Movie) context.getBean("movie");
        System.out.print(obj.getMovieId());
        System.out.print(obj.getMovieName());
        System.out.print(obj.getMovieActor());
    }
}
